/* 1.1 Basandote en el array siguiente, crea una lista ul > li 
dinámicamente en el html que imprima cada uno de los paises.
const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

1.2 Elimina el elemento que tenga la clase .fn-remove-me.

1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos 
en el div de html con el atributo data-function="printHere".
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

1.4 Crea dinamicamente en el html una lista de div que contenga un elemento 
h4 para el titulo y otro elemento img para la imagen.
const countries = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último 
elemento de la lista.

1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los 
elementos de las listas que elimine ese mismo elemento del html.
 */

function runJS() {
    console.log('run');

    countryList();
    removeClases();
    carsList();
    imgAndPhoto();
    removeLastImgAndPhoto();
    buttonsRemoveEveryWhere();
}

window.onload = runJS;


function countryList() {
    const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];
    const newUl = document.createElement('ul');
    countries.forEach(element => {
        const newLi = document.createElement('li');
        newLi.innerText = element;
        newUl.appendChild(newLi);
    });
    document.body.append(newUl);
}

function removeClases() {
    const removeNode = document.querySelector('.fn-remove-me');
    removeNode.remove();
}

function carsList() {
    const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];
    const newUl = document.createElement('ul');
    cars.forEach(element => {
        const newLi = document.createElement('li');
        newLi.innerText = element;
        newUl.appendChild(newLi);
    });
    document.querySelector('[data-function="printHere"]').append(newUl);
}

function imgAndPhoto() {
    const countries = [
        {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'}, 
        {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
        {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
        {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
        {title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
    ];
    countries.forEach(element => {
        const newDiv = document.createElement('div');
        newDiv.setAttribute('class', 'card');
        const newImg = document.createElement('img');
        newImg.setAttribute('src', element.imgUrl);
        newDiv.appendChild(newImg);
        const newTitle = document.createElement('h4');
        newTitle.innerText = element.title;
        newDiv.appendChild(newTitle);
        document.body.append(newDiv);
    });
}

function removeLastImgAndPhoto() {
    const newButton = document.createElement('button');
    newButton.innerText = 'Remove last div';
    document.body.append(newButton);
    newButton.addEventListener('click', function() {
        const divs = document.querySelectorAll('.card');
        if (divs.length > 1) {
            divs[divs.length -1].remove();
        } else {
            divs[divs.length -1].remove();
            newButton.remove();
        }
    });
}

function buttonsRemoveEveryWhere() {
    const lis = document.querySelectorAll('li');
    lis.forEach(li => {
        const textoLi = li.textContent;
        const newButton = document.createElement('button');
        newButton.innerText = 'Remove ' + textoLi;
        newButton.addEventListener('click', function() {
            li.remove();
            newButton.remove();
        });
        document.body.prepend(newButton);
    });
}